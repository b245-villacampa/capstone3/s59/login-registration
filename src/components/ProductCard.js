import {Row, Col, Button, Container, Modal, Table, Form} from 'react-bootstrap'
import{Link, useNavigate} from 'react-router-dom'
import{useContext, useEffect, useState, setShow, Fragment} from 'react'
import Swal from 'sweetalert2'
import UserContext from '../UserContext.js'

export default function ProductCard({productProp}){

	const navigate = useNavigate()

	const {_id, category, name, description, brand, model, regPrice, productPhoto, stocks, isActive} = productProp;

	return(
		<Fragment>
		
	    <tr>
          <td>{category}</td>
          <td><img className="img-fluid" src={productProp.productPhoto}/></td>
          <td>{name}</td>
          <td>{description}</td>
          <td>{brand}</td>
          <td>{model}</td>
          <td>{regPrice}</td>
          <td>{stocks}</td>
          {
          isActive?
          <td>Available</td>
          :
          <td>Not Available</td>
          }
          <td>
          		<Button as = {Link} to={`/adminDashBoardUpdate/${_id}`} variant="primary" >
			     	  Update
			    </Button>
			</td>

        </tr>
        </Fragment>
     
			
		)
}