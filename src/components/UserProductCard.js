import {ButtonGroup, Row, Col, Button, Container, Modal, Table, Form, Offcanvas, Card, Image, ListGroup} from 'react-bootstrap'
import{Link, useNavigate} from 'react-router-dom'
import{useContext, useEffect, useState, setShow, Fragment} from 'react'
import Swal from 'sweetalert2'
import UserContext from '../UserContext.js'

export default function UserProductCard({userProductProp}){

	const navigate = useNavigate()

	const {_id, category, name, description, brand, model, regPrice, productPhoto, stocks, isActive} = userProductProp;
	const {user, setUser} = useContext(UserContext);
	const [show, setShow] = useState(false);
	const [quantity, setQuantity] = useState(1);
	const [isDisabled, setIsDisabled] = useState(false)
	
	function addQuantity(){
			if(quantity >= 1){
				setQuantity(quantity+1);
			}
		}

	function subtractQuantity(){
			if(quantity > 1){
				setQuantity(quantity-1);
			}else{
				setIsDisabled(false);
			}
		}

  	const handleClose = () => setShow(false);
  	const handleShow = () => setShow(true);

  	const [showOffcanvas, setShowOffcanvas] = useState(false);

	const handleCloseShowOffcanvas = () => setShowOffcanvas(false);
	const handleShowOffcanvas = () => setShowOffcanvas(true);

	function createOrder(){
		fetch(`${process.env.REACT_APP_API_URL}/order/createOrder`, {
			method:"POST",
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}, 
			body:JSON.stringify({
				
				productId:_id,
				quantity:quantity
							
			})
		}).then(result=>result.json()).then(data=>{
			if(data){
				Swal.fire({
					title:"Order success!",
					icon:"success",
					text:"Order agaaaaain!"
				})
				navigate('/')
			}
		})
	}

	return(
		<Fragment>
		<Offcanvas show={showOffcanvas} onHide={handleCloseShowOffcanvas}>
		  <Offcanvas.Header closeButton>
		    <Offcanvas.Title>Place your order here</Offcanvas.Title>
		  </Offcanvas.Header>
		  <Offcanvas.Body>
		    <img className="img-fluid offcanvas-img mx-auto d-block" src={productPhoto}/>
		    <h1 className="text-center">{name}</h1>
		    <p><span className="offcanvas-span">Brand:</span> {brand}</p>
		    <p><span className="offcanvas-span">Price:</span> {regPrice}</p>
		    <p><span className="offcanvas-span">Quantity:</span>
		    	<ButtonGroup className="mx-3">
		    	     <Button className="btn btn-sm" variant="outline-danger" onClick={subtractQuantity} disabled={isDisabled}>-</Button>
		    	     <p className="m-3 quantity">{quantity}</p>
		    	     <Button className="btn btn-sm" variant="outline-success" onClick={addQuantity}>+</Button>
		    	</ButtonGroup>
		 	</p>
		    <Button onClick={createOrder}> Place order
		    </Button> 
		  </Offcanvas.Body>
		</Offcanvas>

	    <Col className="col-6 col-md-4">
	    	<Card className="product-column">
	    	      <Card.Img className="card-img" variant="top" src={productPhoto} />
	    	      <Card.Body>
	    	        <Card.Title className="text-center">{name}</Card.Title>
	    	         </Card.Body>
	    	         <ListGroup className="list-group-flush">
	    	                 <ListGroup.Item className="text-center">{category}</ListGroup.Item>
	    	                 <ListGroup.Item className="text-center">{brand}</ListGroup.Item>
	    	                 <ListGroup.Item className="text-center">{model}</ListGroup.Item>
	    	                 <ListGroup.Item className="text-center">PHP {regPrice}</ListGroup.Item>
	    	        </ListGroup>

	    	        <Card.Body>
		    	       <div className="d-grid gap-2 mt-4">
			            <Button variant="primary" type="submit" onClick={handleShow} size="md">
			             See Details
			            </Button>
			            </div>
			            <div className="d-grid gap-2 mt-4">
			            <Button variant="primary" type="submit" size="md" onClick={handleShowOffcanvas}>
			              Add to cart
			            </Button>
			            </div>
		             </Card.Body>

		            <Modal size="lg" aria-labelledby="contained-modal-title-vcenter"centered show={show} onHide={handleClose}>
		                   <Modal.Header closeButton>
		                     <Modal.Title centered>{name}</Modal.Title>
		                   </Modal.Header>
		                   <Modal.Body>
		                   		<Row>
		                   			<Col className="col-6">
		                   				<Image className="img-fluid card-img mx-auto d-block" src={userProductProp.productPhoto}/>
		                   			</Col>
		                   			<Col className="col-6">
		                   				<ListGroup>
		                   				     <ListGroup.Item>Category: {category}</ListGroup.Item>
		                   				     <ListGroup.Item>Brand: {brand}</ListGroup.Item>
		                   				     <ListGroup.Item>Model: {model}</ListGroup.Item>
		                   				     <ListGroup.Item>Description: {description}</ListGroup.Item>
		                   				     <ListGroup.Item>Price: PHP {regPrice}</ListGroup.Item>
		                   				   </ListGroup>
		                   			</Col>
		                   		</Row>
		                   </Modal.Body>
		                   <Modal.Footer>
		                     <Button variant="secondary" onClick={handleClose}>
		                       Close
		                     </Button>
		                   </Modal.Footer>
		                 </Modal>
	    	    </Card>
	    </Col>
        </Fragment>
     
			
		)
}