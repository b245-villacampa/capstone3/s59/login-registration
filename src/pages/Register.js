import {useState, useEffect, useContext} from 'react'
import {Form, Button, Row, Col} from 'react-bootstrap';
import UserContext from '../UserContext.js'
import {useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'

export default function Register(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");

	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(()=>{
		if(firstName !=="" && lastName !=="" && password !== "" && confirmPassword !== "" && email !== "" && password === confirmPassword){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password, confirmPassword, firstName, lastName])


	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
			method:"POST",
			headers:{
				'Content-Type' : 'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password,
				firstName:firstName,
				lastName:lastName
				
			})
		}).then(result=> result.json()).then(data=>{
			if(data){
				Swal.fire({
					title:"Great! You're registered!",
					icon:"success",
					text:"Try to login!"
				})
				navigate("/login");
			}else if(data === false){
				Swal.fire({
					title:"Registration Failed!",
					icon:"error",
					text:"Email is already taken. Please try another email!"
				})
			}
		})

	}


	
	return(

		<Form className="mx-auto mt-3 registerForm" onSubmit={event=>register(event)}>
			<h1 className="text-center mb-3">Register</h1>
			<Row>
				<Col className="col-6">
					<Form.Group className="mb-3" controlId="formBasicRegisterFirstName">
					  <Form.Label>First Name</Form.Label>
					  <Form.Control 
					  	type="text" 
					  	placeholder="Enter First Name"
					  	value={firstName} 
					  	onChange={event=>setFirstName(event.target.value)}
					  	required/>
					</Form.Group>
				</Col>
				<Col className="col-6">
					<Form.Group className="mb-3" controlId="formBasicRegisterLastName">
					  <Form.Label>Last Name</Form.Label>
					  <Form.Control 
					  	type="text" 
					  	placeholder="Enter Last Name"
					  	value={lastName} 
					  	onChange={event=>setLastName(event.target.value)}
					  	required/>
					</Form.Group>
				</Col>	
			</Row>
		    <Form.Group className="mb-3" controlId="formBasicRegisterEmail">
		        <Form.Label>Email address</Form.Label>
		        <Form.Control 
		        	type="email" 
		        	placeholder="Enter email"
		        	value={email} 
		        	onChange={event=>setEmail(event.target.value)}
		        	required/>
		    </Form.Group>

		    <Form.Group className="mb-3" controlId="formBasicRegisterPassword">
		        <Form.Label>Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Enter Password" 
		        	value={password} 
		        	onChange={event=>setPassword(event.target.value)}
		        	required/>
		    </Form.Group>	      

		      <Form.Group className="mb-3" controlId="formBasicRegisterConfirmPassword">
		        <Form.Label>Confirm Password</Form.Label>
		        <Form.Control 
		        	type="password" 
		        	placeholder="Confirm Password"
		        	value={confirmPassword} 
		        	onChange={event=>setConfirmPassword(event.target.value)}
		        	required/>
		      </Form.Group>
		      {
		      	isActive ?
		      <div className="d-grid gap-2 mt-4">
		            <Button variant="primary" type="submit" size="md">
		              Submit
		            </Button>
		      </div>
		      :
		      <div className="d-grid gap-2 mt-4">
		            <Button variant="primary" type="submit" size="md" disabled>
		              Submit
		            </Button>
		      </div>
		  	}
		    </Form>

	)
} 