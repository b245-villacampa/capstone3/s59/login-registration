import {Form, Button, Container} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import Swal from 'sweetalert2';

export default function Login(){

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isDisabled, setIsDisabled] = useState(false);

	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();

	useEffect(()=>{
		if(email !== "" && password !==""){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}
	}, [email, password]);


	function login(event){
		event.preventDefault();
		
		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method:"POST",
			headers:{
				'Content-Type':'application/json'
			},
			body:JSON.stringify({
				email:email,
				password:password
			})
		}).then(result => result.json()).then(data=>{
			

			if(data){
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));
				
			}else{
				
				Swal.fire({
					title:"Login Failed!",
					icon:"error",
					text:"Email or Password is wrong, Please try again!"
				})
			}
		})
	}
		const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/user/profile`, {
			headers:{
				Authorization:`Bearer ${token}`
			}
		}).then(result => result.json()).then(data =>{
			
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			});
			
				if(data.isAdmin){
					Swal.fire({
						title:"Login Success!",
						icon:"success",
						text:"Welcome back admin!"
					})
								
					navigate('/adminDashBoardProduct');
				}else{
					Swal.fire({
						title:"Login Success!",
						icon:"success",
						text:"Enjoy Shopping!"
					})
								
						navigate('/');
				}
			})
		}

	

	return(

		<Container className="form-bg p-5 mt-5">
			<Form className="form mx-auto reg-form" onSubmit={event=>login(event)}>
				<h1 className="text-center mb-3">Login</h1>
			     <Form.Group className="mb-3" controlId="formBasicLoginEmail">
			       <Form.Label>Email address</Form.Label>
			       <Form.Control 
			       		type="email" 
			       		placeholder="Enter email"
			       		value={email}
			       		onChange={event => setEmail(event.target.value)}
			       		required/>
			       <Form.Text className="text-muted">
			         We'll never share your email with anyone else.
			       </Form.Text>
			     </Form.Group>

			     <Form.Group className="mb-3" controlId="formBasicLoginPassword">
			       <Form.Label>Password</Form.Label>
			       <Form.Control 
			       		type="password" 
			       		placeholder="Password" 
			       		value = {password}
			       		onChange={event=> setPassword(event.target.value)}
			       		required/>
			     </Form.Group>
			     {
			     	isDisabled?
			     	<Button variant="secondary" type="submit" disabled>
			     	  Submit
			     	</Button>
			     	:
			     	<Button variant="primary" type="submit">
			     	  Submit
			     	</Button>
			     }
			     
			   </Form>
		</Container>
		

	)
}