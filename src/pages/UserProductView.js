import {Fragment, useState, useEffect} from 'react'
import UserProductCard from '../components/UserProductCard.js'
import {Row, Container} from 'react-bootstrap'


export default function UserProductView(){

const [products, setProducts] = useState([]);

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/product/active`)
		.then(result => result.json())
		.then(data =>{
				setProducts(data.map( product => {
					return (

					<UserProductCard key= {product._id} userProductProp = {product}/>
					)
				}))
		})
	}, [])


	return(

		<Fragment>
		<h1 className="text-center">Products</h1>
 
			<Container className="product-container">
				<Row>
				{products}
				</Row>
			</Container>	
			
		</Fragment>
		)
}